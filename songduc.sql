-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 18, 2023 at 06:35 PM
-- Server version: 10.4.25-MariaDB
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project_2023`
--

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` int(255) NOT NULL,
  `order_code` int(255) UNSIGNED NOT NULL,
  `total_order` int(255) NOT NULL,
  `bank_code` varchar(255) NOT NULL,
  `vnp_BankTranNo` varchar(255) NOT NULL,
  `vnp_CardType` varchar(255) NOT NULL,
  `vnp_OrderInfo` varchar(255) NOT NULL,
  `vnp_PayDate` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_details`
--

CREATE TABLE `product_details` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_product` int(255) UNSIGNED NOT NULL,
  `size` varchar(11) NOT NULL,
  `qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_details`
--

INSERT INTO `product_details` (`id`, `id_product`, `size`, `qty`) VALUES
(449, 922, 'M', 12),
(450, 922, 'S', 12),
(451, 922, 'L', 8),
(452, 718, 'M', 8),
(453, 718, 'S', 8),
(454, 718, 'L', 10),
(455, 617, 'M', 5),
(456, 617, 'S', 14),
(457, 617, 'L', 14),
(458, 958, 'M', 20),
(459, 958, 'S', 12),
(460, 958, 'L', 16),
(461, 610, 'M', 14),
(462, 610, 'S', 14),
(463, 610, 'L', 10),
(464, 541, 'M', 24),
(465, 541, 'S', 30),
(466, 541, 'L', 28),
(467, 291, 'M', 10),
(468, 291, 'S', 18),
(469, 291, 'L', 6),
(470, 127, 'M', 8),
(471, 127, 'S', 16),
(472, 127, 'L', 20),
(473, 892, 'M', 15),
(474, 892, 'S', 12),
(475, 892, 'L', 20),
(476, 850, 'M', 8),
(477, 850, 'S', 10),
(478, 850, 'L', 12),
(479, 726, 'M', 8),
(480, 726, 'S', 6),
(481, 726, 'L', 10),
(482, 474, 'M', 20),
(483, 474, 'S', 20),
(484, 474, 'L', 18);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_comment`
--

CREATE TABLE `tbl_comment` (
  `comments_id` int(11) NOT NULL,
  `customer_id` int(255) UNSIGNED NOT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `content_comment` varchar(255) NOT NULL,
  `comment_date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact`
--

CREATE TABLE `tbl_contact` (
  `contact_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `message` varchar(255) NOT NULL,
  `customers_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_contact`
--

INSERT INTO `tbl_contact` (`contact_id`, `name`, `email`, `phone`, `subject`, `message`, `customers_id`) VALUES
(25, 'admin ', 'theduc@gmail.com', '2147483647', 'update', 'update', 75);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customers`
--

CREATE TABLE `tbl_customers` (
  `customers_id` int(255) UNSIGNED NOT NULL,
  `customers_name` varchar(255) NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `password` varchar(150) NOT NULL,
  `phone` int(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_customers`
--

INSERT INTO `tbl_customers` (`customers_id`, `customers_name`, `avatar`, `password`, `phone`, `email`, `address`, `level`) VALUES
(75, 'admin', 'public/upload/users/avatar_64b677b614603.jpg', 'e10adc3949ba59abbe56e057f20f883e', 2147483647, 'theduc@gmail.com', 'Công an hà nội', 1),
(86, 'tee', 'public/upload/users/avatar_64b67b108c462.jpg', '202cb962ac59075b964b07152d234b70', 853832873, '123@gmail.com', 'Hoàng Mai, Hà Nội', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order`
--

CREATE TABLE `tbl_order` (
  `order_code` int(255) UNSIGNED NOT NULL,
  `order_date` varchar(100) NOT NULL,
  `order_status` int(100) NOT NULL,
  `cancel` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `order_note` varchar(255) NOT NULL,
  `customers_id` int(255) UNSIGNED NOT NULL,
  `payment_methods` varchar(255) NOT NULL,
  `transaction` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_order`
--

INSERT INTO `tbl_order` (`order_code`, `order_date`, `order_status`, `cancel`, `name`, `address`, `email`, `phone`, `order_note`, `customers_id`, `payment_methods`, `transaction`) VALUES
(164, '2023-07-18', 0, 0, 'admin', 'Công an hà nội', 'theduc@gmail.com', '2147483647', 'a', 75, 'Tiền mặt', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order_details`
--

CREATE TABLE `tbl_order_details` (
  `order_code` int(255) UNSIGNED NOT NULL,
  `order_details_id` int(11) UNSIGNED NOT NULL,
  `order_product_id` int(11) UNSIGNED NOT NULL,
  `product_title` varchar(255) NOT NULL,
  `order_product_quanlity` int(11) NOT NULL,
  `size_product` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_order_details`
--

INSERT INTO `tbl_order_details` (`order_code`, `order_details_id`, `order_product_id`, `product_title`, `order_product_quanlity`, `size_product`) VALUES
(164, 672, 617, 'Áo sơ mi nữ hồng tay dài', 1, 'M');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE `tbl_product` (
  `product_id` int(255) UNSIGNED NOT NULL,
  `product_title` varchar(255) NOT NULL,
  `product_price` varchar(255) NOT NULL,
  `product_desc` text NOT NULL,
  `product_image` varchar(100) NOT NULL,
  `category_product_id` int(11) UNSIGNED NOT NULL,
  `status` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`product_id`, `product_title`, `product_price`, `product_desc`, `product_image`, `category_product_id`, `status`) VALUES
(127, 'Áo thun Nam Cleanse', '320000', 'Áo phông nam form cơ bản, dễ mặc,\r\nChất liệu cotton tái chế bảo vệ môi trường, thành phần:  60% Cotton + 20% Polyester + 20% Recycle Polyester,\r\nRất mềm mại, thấm hút thoải mái,\r\nCó hình in đặc trưng cho bộ sưu tập chất liệu tái chế, nâng cao nhận thức môi trường', 'Picture11689669046.png', 30, 1),
(291, 'Áo khoác Nam bo gấu', '460000', 'Chất liệu 90% Nylon, 10% Spandex,\r\nVải nỉ dày dặn phù hợp với thời tiết se lạnh,\r\nChất liệu co giãn 1 lớp khiến sản phẩm vô cùng thoải mái khi sử dụng,\r\nBản bo chun gấu to khỏe khoắn cùng logo nổi bật là điểm nhấn của sản phẩm thêm bắt mắt', 'Picture101689668923.png', 25, 1),
(474, 'Áo thun nữ Bamboo', '180000', 'Áo thun nữ cổ tròn chất liệu bamboo co giãn tốt mang đến cảm giác thoải mái khi mặc,\r\nThành phần:  95% Viscose + 5% Spandex,\r\nVải thấm hút, siêu mềm mại, kháng khuẩn,\r\nKiểu dáng cơ bản phù hợp với nhiều dáng người, nhiều hoàn cảnh', 'áo thun nữ1689670288.png', 30, 1),
(541, 'Áo thun đen Cooling', '300000', 'Áo thun nam chất liệu mát mịn, mềm mại tạo cảm giác thoải mái khi tiếp xúc với da,\r\nThông thoáng, thấm hút tốt nhờ công nghệ hoàn tất hiện đại,\r\nSản phẩm có khả năng co giãn đàn hồi tuyệt vời nhờ kết cấu thành phần sợi,\r\nKhông bai gião, không xù lông, độ bền cao', 'download_aothun1689668626.jpg', 30, 1),
(610, 'Váy nữ Sorona', '280000', 'Chất liệu vải xốp,\r\nThoải mái với vải co giãn, không nhăn, nhanh khô,\r\nHọa tiết hoa nổi phối tơ giúp thiết kế trở nên trẻ trung, nữ tính,\r\nPhần tay bồng giúp che đi khuyết điểm bắp tay,\r\nĐiểm nhấn là 1 chiếc cúc đá hoa đính trên cổ áo giúp chiếc đầm trở nên tinh tế và cuôn hút hơn.', 'img_vaynu1689668444.jpg', 28, 1),
(617, 'Áo sơ mi nữ hồng tay dài', '340000', 'Áo sơ mi nữ tay dài dáng rộng trẻ trung,\r\nChất liệu:  97%Cotton + 3%Spandex,\r\nCổ bẻ, khuy dọc thân trước, cầu vai phía sau và một túi ngực mở,\r\nVai ráp trễ, tay dài với măng sét cài khuy và vạt hơi tròn. Thân sau hơi dài hơn', 'img_aosominu1689668089.jpg', 26, 1),
(718, 'Áo khoác Bomber', '600000', ' Áo khoác bomber nam vải gió giúp giữ ấm cơ thể tốt,\r\nThành phần:  90% Nylon, 10% Spandex,\r\nKiểu dáng thời trang cùng màu sắc trẻ trung, năng động, dễ dàng phối với nhiều trang phục khác nhau,\r\nThiết kế bo len cổ, tay và gấu áo tạo độ ôm vừa phải mang đến cảm giác thoải mái khi mặc,\r\nTay áo có túi khóa tạo sự khỏe khoắn và để đồ tiện lợi, chắc chắn', 'img_aosominam1689667960.jpg', 25, 1),
(726, 'Bộ đồ ngủ Nữ Enwear', '500000', 'Bộ đồ ngủ với chất vải cotton mượt mà,\r\nChất liệu nhẹ nhàng, mềm mịn mặc siêu mát,\r\nĐồ ngủ mang đến cảm giác thoải mái nhất khi hoạt động,\r\nĐộ bền cao, ít nhăn nhàu', 'bộ đồ ngủ nữ1689669936.jpg', 31, 1),
(850, 'Đầm đen công sở', '520000', 'Váy lụa thanh lịch, sang trọng,\r\nChất liệu 100% Polyester mềm mại,\r\nThiết kế nữ tính, khéo léo khoe đường cong cơ thể,\r\nĐộ bóng nhẹ giúp chị em lên đồ thanh lịch, phù hợp đi làm, dự tiệc hay đi chơi', 'váy nữ1689669648.jpg', 28, 1),
(892, 'Áo khoác Nữ Kaki', '500000', 'Chất liệu sản phẩm: Vải kaki,\r\nThành phần: 97% Cotton + 3% Spandex,\r\nĐường may và sợi vải chắc chắn, thân thiện với người dùng,\r\nÁo khoác nhẹ trong thời tiết sẽ lạnh cũng như chống nắng  ', 'Picture121689669219.png', 25, 1),
(922, 'Áo sơ mi nam trắng ', '350000', 'Chất liệu vải bamboo - sợi tre tiên tiến, thân thiện với người dùng,\r\nThành phần:  30% Viscose by BAMBOO 65% POLYESTER 5% SPANDEX,\r\nThoáng mát ngày hè và có tính hút ẩm rất cao, thấm hút mồ hôi tốt hơn rất nhiều so với sợi vải cotton,\r\nChống lại tia cực tím hay tia UV cực kỳ cao, cực kỳ hiểu hiệu quả,\r\nThiết kế lịch lãm, kẻ caro tỉ mỉ tạo nên phối đồ lịch sự     ', 'download_aosomi21689654994.jpg', 26, 1),
(958, 'Bộ đồ ngủ Nam Vincy', '500000', 'Bộ pijama với chất vải cotton hàng quảng châu siêu đẹp,\r\nChất liệu nhẹ nhàng, mềm mịn mặc siêu mát,\r\nĐồ ngủ sử dụng chất liệu cao cấp mang đến cảm giác thoải mái nhất khi hoạt động,\r\nĐộ bền cao, ít nhăn nhàu\r\n ', 'images_doaongu1689668319.jpg', 31, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_statistic`
--

CREATE TABLE `tbl_statistic` (
  `id` int(11) NOT NULL,
  `order_date` varchar(255) NOT NULL,
  `quanlity` int(255) NOT NULL,
  `sales` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_statistic`
--

INSERT INTO `tbl_statistic` (`id`, `order_date`, `quanlity`, `sales`) VALUES
(9, '2023-03-17', 1, 200000),
(10, '2022-03-18', 9, 150000),
(11, '2023-01-18', 1, 600000),
(12, '2023-03-19', 2, 40000),
(13, '2022-03-19', 1, 50000),
(14, '2023-04-03', 1, 200000),
(15, '2002-04-03', 1, 2000000),
(16, '2023-04-09', 1, 15000),
(17, '2023-04-19', 1, 200000),
(18, '2023-04-19', 1, 200000),
(19, '2023-04-19', 1, 200000),
(20, '2023-02-09', 1, 200000),
(21, '2023-04-22', 1, 200000),
(22, '2023-04-22', 1, 200000),
(23, '2023-01-22', 1, 200000),
(24, '2023-07-18', 3, 450000),
(25, '2023-07-18', 1, 500000),
(26, '2023-07-18', 1, 500000);

-- --------------------------------------------------------

--
-- Table structure for table `tb_category_product`
--

CREATE TABLE `tb_category_product` (
  `category_product_id` int(11) UNSIGNED NOT NULL,
  `category_product_title` varchar(255) NOT NULL,
  `category_product_desc` varchar(150) NOT NULL,
  `category_product_images` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_category_product`
--

INSERT INTO `tb_category_product` (`category_product_id`, `category_product_title`, `category_product_desc`, `category_product_images`) VALUES
(25, 'Áo khoác', 'Jacket', 'ao_khoac_cate.jpg'),
(26, 'Áo sơ mi', 'Shirt', 'images_somi.jpg'),
(28, 'Váy nữ', 'Dress', 'váy_nữ.jpg'),
(30, 'Áo thun', 'T-shirt', 'aopolo.jpg'),
(31, 'Bộ đồ ngủ', 'Pajamas', 'pajamas.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_code` (`order_code`);

--
-- Indexes for table `product_details`
--
ALTER TABLE `product_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_product` (`id_product`);

--
-- Indexes for table `tbl_comment`
--
ALTER TABLE `tbl_comment`
  ADD PRIMARY KEY (`comments_id`),
  ADD KEY `customer_id` (`customer_id`,`product_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `tbl_contact`
--
ALTER TABLE `tbl_contact`
  ADD PRIMARY KEY (`contact_id`),
  ADD KEY `customers_id` (`customers_id`),
  ADD KEY `customers_id_2` (`customers_id`),
  ADD KEY `customers_id_3` (`customers_id`);

--
-- Indexes for table `tbl_customers`
--
ALTER TABLE `tbl_customers`
  ADD PRIMARY KEY (`customers_id`);

--
-- Indexes for table `tbl_order`
--
ALTER TABLE `tbl_order`
  ADD PRIMARY KEY (`order_code`),
  ADD KEY `customer_id` (`customers_id`);

--
-- Indexes for table `tbl_order_details`
--
ALTER TABLE `tbl_order_details`
  ADD PRIMARY KEY (`order_details_id`),
  ADD KEY `product_id` (`order_product_id`),
  ADD KEY `order_code` (`order_code`);

--
-- Indexes for table `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `category_product_id` (`category_product_id`);

--
-- Indexes for table `tbl_statistic`
--
ALTER TABLE `tbl_statistic`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_date` (`order_date`);

--
-- Indexes for table `tb_category_product`
--
ALTER TABLE `tb_category_product`
  ADD PRIMARY KEY (`category_product_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `product_details`
--
ALTER TABLE `product_details`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=485;

--
-- AUTO_INCREMENT for table `tbl_comment`
--
ALTER TABLE `tbl_comment`
  MODIFY `comments_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tbl_contact`
--
ALTER TABLE `tbl_contact`
  MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `tbl_customers`
--
ALTER TABLE `tbl_customers`
  MODIFY `customers_id` int(255) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT for table `tbl_order_details`
--
ALTER TABLE `tbl_order_details`
  MODIFY `order_details_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=674;

--
-- AUTO_INCREMENT for table `tbl_statistic`
--
ALTER TABLE `tbl_statistic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `tb_category_product`
--
ALTER TABLE `tb_category_product`
  MODIFY `category_product_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `payment`
--
ALTER TABLE `payment`
  ADD CONSTRAINT `payment_ibfk_1` FOREIGN KEY (`order_code`) REFERENCES `tbl_order` (`order_code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_details`
--
ALTER TABLE `product_details`
  ADD CONSTRAINT `product_details_ibfk_1` FOREIGN KEY (`id_product`) REFERENCES `tbl_product` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_comment`
--
ALTER TABLE `tbl_comment`
  ADD CONSTRAINT `tbl_comment_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `tbl_customers` (`customers_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_comment_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `tbl_product` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_contact`
--
ALTER TABLE `tbl_contact`
  ADD CONSTRAINT `tbl_contact_ibfk_1` FOREIGN KEY (`customers_id`) REFERENCES `tbl_customers` (`customers_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_order`
--
ALTER TABLE `tbl_order`
  ADD CONSTRAINT `tbl_order_ibfk_1` FOREIGN KEY (`customers_id`) REFERENCES `tbl_customers` (`customers_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_order_details`
--
ALTER TABLE `tbl_order_details`
  ADD CONSTRAINT `tbl_order_details_ibfk_2` FOREIGN KEY (`order_code`) REFERENCES `tbl_order` (`order_code`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_order_details_ibfk_3` FOREIGN KEY (`order_product_id`) REFERENCES `tbl_product` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD CONSTRAINT `tbl_product_ibfk_1` FOREIGN KEY (`category_product_id`) REFERENCES `tb_category_product` (`category_product_id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
