<nav aria-label="breadcrumb" style="font-size: 30px;">
                    <ol class="breadcrumb"style="font-size: 30px;font-family: system-ui;">
                        <li class="breadcrumb-item"><a href="<?php echo BASE_URL ?>/login/dashboard">Trang chủ</a></li>
                        <li class="breadcrumb-item active" aria-current="page" >Danh sách bình luận</li>
                    </ol>
                </nav>
<h3 style="text-align: center; font-size: 35px;margin: 30px 0;">Danh sách bình luận</h3><table class="table table-striped">
    
<thead>
      <tr>
      <th class="text-center h2">STT</th>
        <th class="text-center h2">ID Comment</th>
        <th class="text-center h2">ID Người dùng</th>
        <th class="text-center h2">ID Sản phẩm</th>
        <th class="text-center h2">Nội dung</th>
        <th class="text-center h2">Ngày bình luận</th>
      </tr>
    </thead>
    <tbody>
    <?php
    $i=0;
        foreach ($comment as $key => $cmt){
            $i++;
    ?>  
      <tr>
        <td class="text-center h4"><?php echo $i ?></td>
        <td class="text-center h4"><?php echo $cmt['comments_id'] ?></td>
        <td class="text-center h4"><?php echo $cmt['customer_id'] ?></td>
        <td class="text-center h4"><?php echo $cmt['product_id'] ?></td>
        <td class="text-center h4"><?php echo $cmt['content_comment'] ?></td>
        <td class="text-center h4"><?php echo $cmt['comment_date'] ?></td>
      </tr>
      <?php
        }
         ?>
    </tbody>
