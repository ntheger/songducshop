<nav aria-label="breadcrumb" style="font-size: 30px;">
                    <ol class="breadcrumb"style="font-size: 30px;font-family: system-ui;">
                        <li class="breadcrumb-item"><a href="<?php echo BASE_URL ?>/login/dashboard">Trang chủ</a></li>
                        <li class="breadcrumb-item active" aria-current="page" >Danh sách liên hệ</li>
                    </ol>
                </nav>
<h3 style="text-align: center; font-size: 35px;margin: 30px 0;">Danh sách liên hệ</h3><table class="table table-striped">
    
<thead>
      <tr>
      <th class="text-center h2">STT</th>
        <th class="text-center h2">Tên</th>
        <th class="text-center h2">Email</th>
        <th class="text-center h2">Số điện thoại</th>
        <th class="text-center h2">Tiêu đề</th>
        <th class="text-center h2">Tin nhắn</th>
      </tr>
    </thead>
    <tbody>
    <?php
    $i=0;
        foreach ($contact as $key => $cont){
            $i++;
    ?>  
      <tr>
        <td class="text-center h4"><?php echo $i ?></td>
        <td class="text-center h4"><?php echo $cont['name'] ?></td>
        <td class="text-center h4"><?php echo $cont['email'] ?></td>
        <td class="text-center h4"><?php echo $cont['phone'] ?></td>
        <td class="text-center h4"><?php echo $cont['subject'] ?></td>
        <td class="text-center"><textarea class="text-center h4" readonly="true" style="background-color: #f9f9f9;"><?php echo $cont['message'] ?></textarea></td>
       
      </tr>
      <?php
        }
         ?>
    </tbody>
