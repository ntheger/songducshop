<?php
if(!empty($_GET['msg'])){
  $msg= unserialize(urldecode($_GET['msg']));
  foreach($msg as $key => $value){
    echo '<div class="">
            <h3>Notification</h>
            <div class="alert alert-success">
              <h4>'.$value.'</h4> 
            </div>
          </div>';
  }
}
?>
<nav aria-label="breadcrumb" style="font-size: 30px; font-family: system-ui;">
  <ol class="breadcrumb"style="font-size: 30px; font-family: system-ui;">
    <li class="breadcrumb-item"><a href="<?php echo BASE_URL ?>/login/dashboard">Trang chủ</a></li>
    <li class="breadcrumb-item"><a href="<?php echo BASE_URL ?>/product/list_category">Danh sách loại sản phẩm</a></li>
    <li class="breadcrumb-item active" aria-current="page">Chỉnh sửa loại sản phẩm</li>
  </ol>
</nav>

<h3 style="text-align: center; font-size: 35px;margin: 30px 0;">Cập nhật loại sản phẩm</h3>

<div class="mx-auto" style="font-size: 30px; font-family: system-ui;">
  <?php
  foreach($categorybyid as $key => $cate){
  ?>
  <form action="<?php echo BASE_URL ?>/product/update_category/<?php echo $cate['category_product_id']?>" method="post" enctype="multipart/form-data">
    <div class="form-group">
      <label>Tên loại sản phẩm</label>
      <input type="text" value="<?php echo $cate['category_product_title'] ?>" class="form-control" name="category_product_title" placeholder="Category name">
    </div>
    <div class="form-group">
      <label>Mô tả loại sản phẩm</label>
      <textarea class="form-control" placeholder="Category description" name="category_product_desc" style="resize: none;" rows="5"><?php echo $cate['category_product_desc'] ?></textarea>
    </div>
    <div class="form-group">
        <label>Hình ảnh loại sản phẩm</label>
        <img src="<?php echo BASE_URL ?>/public/upload/category/<?php echo $cate['category_product_images'] ?>" alt="Category Image" class="img-fluid">
    </div>
    <div class="form-group">
        <label>Chọn hình ảnh mới</label>
        <input type="file" class="form-control" name="new_category_product_images" placeholder="New Category Image">
    </div>

    <button type="submit" class="btn btn-primary">Cập nhật</button>
  </form>
  <?php
  }
  ?>
</div>