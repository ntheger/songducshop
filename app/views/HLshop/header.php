<!DOCTYPE html>
<html lang="en">

<head>
  <title>SDShop</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="icon" type="image/png" href="/public/images/logo_sdshop.png">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,700">
  <link rel="stylesheet" href="<?php echo BASE_URL ?>/public/fonts/icomoon/style.css">
  <link rel="stylesheet" href="<?php echo BASE_URL ?>/public/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo BASE_URL ?>/public/css/magnific-popup.css">
  <link rel="stylesheet" href="<?php echo BASE_URL ?>/public/css/jquery-ui.css">
  <link rel="stylesheet" href="<?php echo BASE_URL ?>/public/css/owl.carousel.min.css">
  <link rel="stylesheet" href="<?php echo BASE_URL ?>/public/css/owl.theme.default.min.css">

  <link rel="stylesheet" href="<?php echo BASE_URL ?>/public/css/aos.css">

  <link rel="stylesheet" href="<?php echo BASE_URL ?>/public/css/style.css">
  <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  <script src="<?php echo BASE_URL ?>/public/js/bootstrap.min.js"></script>
  <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet"> 

    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="<?php echo BASE_URL ?>lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="<?php echo BASE_URL ?>/css/style.css" rel="stylesheet">
</head>

<body>
  <style>
    a {
    font-size: 15px !important;
    color: #11006F;
    text-decoration: none;
    background-color: transparent;
    }
    .dropbtn {
      font-family: 'Roboto', sans-serif;
      background-color: #04AA6D;
      color: white;
      padding: 16px;
      font-size: 15px;
      border: none;
      cursor: pointer;
    }

    .dropbtn:hover,
    .dropbtn:focus {
      background-color: #3e8e41;
    }

    .dropdown {
      /* float: left; */
      position: relative;
      display: inline-block;
    }
    .dropdown-menu{
      padding: 20px 30px !important;
    }
    .dropdown-menu li{
      margin-top: 15px;
    }
    .dropdown-content {
      display: none;
      position: absolute;
      background-color: #f1f1f1;
      min-width: 200px;
      overflow: auto;
      box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
      right: 0;
      z-index: 1;
    }

    .dropdown-content a {
      color: black;
      padding: 12px 16px;
      text-decoration: none;
      display: block;
    }

    .dropdown a:hover {
     color: #ccc;
    }

    .show {
      display: block;
    }
    .icons{
      height: 80px;
    display: flex;
    align-content: center;
    align-items: center;
    }
    .icons a{
      margin-right: 25px !important;
    }
    #banner {
      background-color: #f1f1f1;
      width: 1918px;
      height: 77px;
      overflow: hidden;
      position: relative;
    }


    .banner-content {
      display: flex;
      /* animation: changeBanner 10s infinite; Set the duration for banner rotation (10 seconds in this case) */
    }

  .banner-item {
      flex: 1;
      padding: 5px;
      text-align: center;
      width: 100%; /* Make sure each banner item takes the full width of the container */
      height: 100%; /* Make sure each banner item takes the full height of the container */
    }


  /* CSS animation to transition between banner items */
  @keyframes changeBanner {
    0%, 100% {
      transform: translateX(0);
    }
    20% {
      transform: translateX(-100%);
    }
    40% {
      transform: translateX(-200%);
    }
    /* Add more keyframes for additional banner items */
  }
  </style>

  <!-- Bannner Start -->
  <div id="banner" class="container-fluid">
    <div class="banner-content">
      <div class="banner-item"><img src="/public/images/banner_1.jpg" alt=""></div>
      <div class="banner-item"><img src="/public/images/banner_2.jpg" alt=""></div>
    </div>
  </div>

  <!-- End Bannner Start -->
  <!-- Topbar Start -->
  <div class="container-fluid">
        <div class="row align-items-center py-3 px-xl-5" style="font-size: 13px;">
            <div class="col-lg-3 d-none d-lg-block">
                <a href="<?php echo BASE_URL ?>/index/homepage" class="text-decoration-none">
                    <h1 class="m-0 display-5 font-weight-semi-bold"><span class="text-primary font-weight-bold border px-3 mr-1">SD</span>Shop</h1>
                </a>
            </div>
            <div class="col-lg-6 col-6 text-left" style="font-size: 13px;">
              <form action="<?php echo BASE_URL ?>/index/search?quanli=timkiem" method="POST">
                    <div class="input-group">
                        <input type="text" name="tukhoa" class="form-control" placeholder="Tìm kiếm sản phẩm....">
                        <div class="input-group-append">
                            <span class="input-group-text bg-transparent text-primary">
                                <i class="fa fa-search"></i>
                            </span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-lg-3 col-6 text-right">
                <!-- <a href="" class="btn border">
                    <i class="fas fa-heart text-primary"></i>
                    <span class="badge">0</span>
                </a> -->
                <a href="<?php echo BASE_URL ?>/cart/cart" class="btn border">
                  <i class="fa-sharp fa-solid fa-cart-shopping"></i>
                  <span>Giỏ h</span>
                </a>
                
            </div>
        </div>
    </div>
    <!-- Topbar End -->
  <div class="site-wrap">
    <div class="site-navbar bg-white py-2">
      <div class="search-wrap">
        <div class="container">
        </div>
      </div>

      <div class="container" style=" font-size: 30px; font-family: system-ui;">
        <div class="d-flex align-items-center justify-content-between">
          <div>
            <div>
              <a href="<?php echo BASE_URL ?>/index/homepage"><img src="<?php echo BASE_URL ?>/public/images/logo_sdshop_min.png" width="50"></a>
            </div>
          </div>
          <div class="main-nav d-none d-lg-block">
            <nav class="site-navigation text-right text-md-center" role="navigation">
              <ul class="site-menu js-clone-nav d-none d-lg-block">
                <li><a href="<?php echo BASE_URL ?>/index/homepage">Trang chủ</a></li>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown">Sản phẩm<span></span></a>
                  <ul class="dropdown-menu">
                    <a href="<?php echo BASE_URL ?>/sanpham/tatca">Tất cả sản phẩm</a>
                    <?php
                    foreach ($category as $key => $cate) {
                    ?>
                      <li>
                        <a href="<?php echo BASE_URL ?>/sanpham/danhmuc/<?php echo $cate['category_product_id'] ?> "><?php echo $cate['category_product_title'] ?></a>

                      </li>
                    <?php
                    }
                    ?>
                  </ul>
                </li>
                <li>

                  <?php
                  if (Session::get('customer')) {
                    $id = Session::get('customers_id');
                  ?>

                <li><a href="<?php echo BASE_URL ?>/order/customer_order/<?php echo $id ?>">Đặt hàng</a></li>
              <?php
                  } else {
              ?>
                <li><?php echo '' ?></li>
              <?php
                  }
              ?>

              <li><a href="<?php echo BASE_URL ?>/contact/contact">Liên hệ</a></li>
              </ul>
            </nav>
          </div>
          <div class="icons btn" style="font-size: 26px;">
            <!-- <a href="<?php echo BASE_URL ?>/index/search" value="timkiem" class="icons-btn m-2 d-inline-block js-search-open"><span class="icon-search"></span></a> -->
            <!-- <a href="<?php echo BASE_URL ?>/cart/cart" class="icons-btn d-inline-block bag">
              <span class="icon-shopping-bag"></span>
            </a> -->
            <a class="icons-btn  d-inline-block" href="<?php echo BASE_URL ?>/customer/logout"><i class="fa-solid fa-right-from-bracket"></i> </a>
            <?php
            if (!Session::get('customer')) {
            ?>
              <a href="<?php echo BASE_URL ?>/customer/customer_signin" class="icons-btn m-2  d-inline-block"><i class="fa-solid fa-user"></i> </a>

            <?php
            } else {
            ?>
              <div class="icons btn btn-sm" style="width: 50px; height: 60px">
                <?php
                $name =  Session::get('customers_name');
                ?>
                <div class="dropdown">
                <button style="font-size: 13px;" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false">
                    <?php echo $name ?>
                  </button>
                  <ul class="dropdown-menu">
                      <li><a class="dropdown-item" href="<?php echo BASE_URL ?>/login/dashboard">Admin</a></li>
                      <li><a class="dropdown-item" href="<?php echo BASE_URL ?>/customer/profile">Hồ sơ</a></li>
                      <li><a class="dropdown-item" href="<?php echo BASE_URL ?>/customer/logout">Đăng xuất</a></li>
                  </ul>

                </div>
              </div>

            <?php
            }
            ?>

            <a href="#" class="site-menu-toggle js-menu-toggle ml-3 d-inline-block d-lg-none"><span class="icon-menu"></span></a>
          </div>
        </div>
      </div>
    </div>

  <script> 
    window.addEventListener('DOMContentLoaded', (event) => {
      // Lấy phần tử mục "Sản phẩm"
      var dropdown = document.querySelector('.dropdown');

      // Gắn kết sự kiện 'mouseover' vào mục "Sản phẩm"
      dropdown.addEventListener('mouseover', function() {
        // Hiển thị dropdown-content khi di chuột vào mục "Sản phẩm"
        var dropdownContent = this.querySelector('.dropdown-menu');
        dropdownContent.style.display = 'block';
      });

      // Gắn kết sự kiện 'mouseout' vào mục "Sản phẩm"
      dropdown.addEventListener('mouseout', function() {
        // Ẩn dropdown-content khi di chuột ra khỏi mục "Sản phẩm"
        var dropdownContent = this.querySelector('.dropdown-menu');
        dropdownContent.style.display = 'none';
      });
    });
  </script>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="<?php echo BASE_URL ?>/public/js/bootstrap.min.js"></script>
    <script src="<?php echo BASE_URL ?>lib/owlcarousel/owl.carousel.min.js"></script>
    <!-- Contact Javascript File -->
    <script src="<?php echo BASE_URL ?>/mail/jqBootstrapValidation.min.js"></script>
    <script src="<?php echo BASE_URL ?>/mail/contact.js"></script>
    <!-- Template Javascript -->
    <script src="<?php echo BASE_URL ?>/public/js/main_index.js"></script>
    <script>
      // JavaScript to rotate the banner
      const bannerItems = document.querySelectorAll('.banner-item');
      const bannerWidth = 1890;
      const bannerHeight = 70;
      let currentIndex = 0;

      function showNextBanner() {
        bannerItems[currentIndex].style.display = 'none';
        currentIndex = (currentIndex + 1) % bannerItems.length;
        bannerItems[currentIndex].style.display = 'block';
      }

      // Initially hide all banner items except the first one
      for (let i = 1; i < bannerItems.length; i++) {
        bannerItems[i].style.display = 'none';
      }
      document.getElementById('banner').style.width = bannerWidth + 'px';
      document.getElementById('banner').style.height = bannerHeight + 'px';

       setInterval(showNextBanner, 1000); // Rotate every 1 second
    </script>



