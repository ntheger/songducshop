<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">
<div class="container-fluid bg-secondary text-dark mt-5 pt-5">
        <div class="row px-xl-5 pt-5">
            <div class="col-lg-4 col-md-12 mb-5 pr-3 pr-xl-5" style="font-size: 13px;">
                <a href="/index.php" class="text-decoration-none">
                    <h1 class="mb-4 display-5 font-weight-semi-bold"><span class="text-primary font-weight-bold border border-white px-3 mr-1">SD</span>Shop</h1>
                </a>
                <p>Nơi mang tới cho mọi người những bộ đồ thời trang - thời thượng</p>
                <p class="mb-2"><i class="fa-solid fa-location-dot"></i> Hà Đông, Hà Nội</p>
                <p class="mb-2"><i class="fa-sharp fa-solid fa-envelope"></i> sdshop@gmail.com</p>
                <p class="mb-0"><i class="fa-sharp fa-solid fa-phone-volume"></i> 038 3496 829</p>
            </div>
            <div class="col-lg-8 col-md-12">
                <div class="row">
                    <div class="col-md-4 mb-5" style="font-size: 13px;">
                        
                    </div>
                    <div class="col-md-4 mb-5">
                      <h5 class="font-weight-bold text-dark mb-4">Liên kết nhanh</h5>
                        <div class="d-flex flex-column justify-content-start" style="font-size: 13px;">
                            <a class="text-dark mb-2" href="<?php echo BASE_URL ?>/index/homepage"><i class="fa-solid fa-angle-right"></i> Trang chủ</a>
                            <a class="text-dark mb-2" href="<?php echo BASE_URL ?>/sanpham/tatca"><i class="fa-solid fa-angle-right"></i> Cửa hàng của chúng tôi</a>
                            <a class="text-dark mb-2" href="<?php echo BASE_URL ?>/cart/cart""><i class="fa-solid fa-angle-right"></i> Giỏ hàng</a>
                            <a class="text-dark" href="<?php echo BASE_URL ?>/contact/contact"><i class="fa-solid fa-angle-right"></i> Liên hệ</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row border-top border-light mx-xl-5 py-4">
            <div class="col-md-6 px-xl-0">
                <p class="mb-md-0 text-center text-md-left text-dark" style="font-size: 13px;">
                    &copy; <a class="text-dark font-weight-semi-bold" href="<?php echo BASE_URL ?>/index/homepage">Song Đức Shop</a>. All Rights Reserved.</p>
            </div>
            <div class="col-md-6 px-xl-0 text-center text-md-right">
                <img class="img-fluid" src="img/payments.png" alt="">
            </div>
        </div>
    </div>
</div>

<script src="<?php echo BASE_URL ?>/public/js/jquery-3.3.1.min.js"></script>
<script src="<?php echo BASE_URL ?>/public/js/jquery-ui.js"></script>
<script src="<?php echo BASE_URL ?>/public/js/popper.min.js"></script>
<script src="<?php echo BASE_URL ?>/public/js/bootstrap.min.js"></script>
<script src="<?php echo BASE_URL ?>/public/js/owl.carousel.min.js"></script>
<script src="<?php echo BASE_URL ?>/public/js/jquery.magnific-popup.min.js"></script>
<script src="<?php echo BASE_URL ?>/public/js/aos.js"></script>
<script src="<?php echo BASE_URL ?>/public/js/main.js"></script>

</body>
</html>