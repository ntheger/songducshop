<?php
if (!empty($_GET['msg'])) {
  $msg = unserialize(urldecode($_GET['msg']));
  foreach ($msg as $key => $value) {
    echo '<div class="container">
      <h3>Notification</h3>
      <div class="alert alert-success">
        <h4>' . $value . '</h4> 
      </div>
    </div>';
  }
}

if (isset($_POST['upload'])) {
  $name = $_POST['name'];
  $email = $_POST['email'];
  $password = $_POST['password'];
  $phone = $_POST['phone'];
  $address = $_POST['address'];

  // Lưu thông tin cơ bản vào cơ sở dữ liệu
  // ...

  // Xử lý tải lên hình ảnh
  if (!empty($_FILES['avatar']['name'])) {
    $file_name = $_FILES['avatar']['name'];
    $file_tmp = $_FILES['avatar']['tmp_name'];
    $file_type = $_FILES['avatar']['type'];
    $file_size = $_FILES['avatar']['size'];
    $file_error = $_FILES['avatar']['error'];

    // Kiểm tra kiểu file và kích thước file
    $allowed_extensions = array("jpg", "jpeg", "png");
    $max_file_size = 5 * 1024 * 1024; // 5MB
    $file_extension = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));

    if (in_array($file_extension, $allowed_extensions) && $file_size <= $max_file_size && $file_error == 0) {
      $new_file_name = uniqid('avatar_') . '.' . $file_extension;
      $upload_path = 'public/upload/users/' . $new_file_name;

      if (move_uploaded_file($file_tmp, $upload_path)) {
        // Lưu đường dẫn hình ảnh vào cơ sở dữ liệu
        // ...

        // Hiển thị hình ảnh đã tải lên
        $avatar_path = BASE_URL . '/' . $upload_path;
        echo '<div class="container">
          <h3>Uploaded Avatar</h3>
          <img src="' . $avatar_path . '" alt="Avatar" style="max-width: 200px;">
        </div>';
      } else {
        echo '<div class="container">
          <div class="alert alert-danger">
            <h4>Failed to upload avatar.</h4>
          </div>
        </div>';
      }
    } else {
      echo '<div class="container">
        <div class="alert alert-danger">
          <h4>Invalid file format or size exceeds the limit.</h4>
        </div>
      </div>';
    }
  }
}
?>

<div class="custom-border-bottom py-3">
  <div class="container">
    <div class="row" style="font-size: 18px; font-family: system-ui;">
      <div class="col-md-12 mb-0"><a href="<?php echo BASE_URL ?>/index/homepage">Trang chủ</a> <span class="mx-2 mb-0">/</span> <strong class="text-black">Chỉnh sửa hồ sơ</strong></div>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-md-8" style="font-family: system-ui; font-size:15px">
      <form action="<?php echo BASE_URL ?>/customer/upload_profile" method="post" enctype="multipart/form-data">
        <div class="p-3 p-lg-5 border">
          <?php foreach ($customer as $key => $info) { ?>
            <div class="form-group row">
              <div class="col-md-12">
                <label for="c_fname" class="text-black">Tên tài khoản<span class="text-danger">*</span></label>
                <input type="text" class="form-control" id="c_fname" name="name" value="<?php echo $info['customers_name']; ?>">
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-12">
                <label for="c_fname" class="text-black">Avatar<span class="text-danger">*</span></label>
                <input type="file" class="form-control" name="avatar" placeholder="User Image">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <label for="c_email" class="text-black">Email <span class="text-danger">*</span></label>
                <input type="email" class="form-control" id="c_email" value="<?php echo $info['email']; ?>" name="email">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <label for="c_subject" class="text-black">Mật khẩu <span class="text-danger">*</span> </label>
                <input type="password" class="form-control" id="c_subject" name="password">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <label for="c_subject" class="text-black">Số điện thoại </label>
                <input type="text" class="form-control" id="c_subject" name="phone" value="<?php echo $info['phone']; ?>">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <label for="c_subject" class="text-black">Địa chỉ </label>
                <input type="text" class="form-control" id="c_subject" name="address" value="<?php echo $info['address']; ?>" placeholder="<?php echo $info['address']; ?>">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-lg-12">
                <input type="submit" name="upload" class="btn btn-info btn-lg btn-block" value="Upload">
              </div>
            </div>
          <?php } ?>
        </div>
      </form>
    </div>
    <div class="col-md-4 ml-auto" style="font-family: system-ui; font-size:18px;">
      <div class="p-4 border mb-3">
        <span class="d-block text-info h6 text-uppercase">Cần Thơ</span>
        <h5 class="mb-0">Cần Thơ</h5>
      </div>
      <div class="p-4 border mb-3">
        <span class="d-block text-info h6 text-uppercase">Hà Nội</span>
        <h5 class="mb-0">Hà Nội</h5>
      </div>
      <div class="p-4 border mb-3">
        <span class="d-block text-info h6 text-uppercase">Hồ Chí Minh</span>
        <h5 class="mb-0">Thành Phố Hồ Chí Minh</h5>
      </div>
    </div>
  </div>
</div>
