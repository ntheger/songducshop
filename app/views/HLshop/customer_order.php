<style>
      .font_fix {
      font-family: 'Roboto', sans-serif;
      font-size: 15px;
    }
    a {
      font-size: 15px !important;
      color: #11006F;
      text-decoration: none;
      background-color: transparent;
    }
    h4 {
      font-size: 15px;
    }
  </style>

<div class="container">
  <div class="row">

  </div>
</div>
</div>

<div class="custom-border-bottom py-3">
  <div class="container">
    <div class="row .font_fix">
      <div class="col-md-12 mb-0"><a href="<?php echo BASE_URL ?>/index/homepage/<?php echo BASE_URL ?>">Trang chủ</a> <span class="mx-2 mb-0 font_fix">/</span> <strong class="text-black font_fix">Đặt hàng</strong></div>
    </div>
  </div>
</div>
<div>
  <?php
  if (!empty($_GET['msg'])) {
    $msg = unserialize(urldecode($_GET['msg']));
    foreach ($msg as $key => $value) {
      echo '  <div class="container">
  <h3>Notification</h>
  <div class="alert alert-success font_fix">
    <h4>' . $value . '</h4> 
  </div> ';
    }
  }

  ?>
</div>
<div class="container responsive " style="  font-family: 'Roboto';">
  <h1 class="m-2" style="text-align: center; font-size: 25px;"> Đơn đặt hàng của bạn</h1>

  <table class="table table-striped">
    <thead>
      <tr style="font-size: 15px; font-family: 'Roboto';">
        <th class="text-center"><Span>ID</Span></th>
        <th class="text-center"><Span>Mã đặt hàng</Span></th>
        <th class="text-center"><Span>Ngày đặt hàng</Span></th>
        <th class="text-center"><Span>Quản lý</Span></th>
        <th class="text-center"><Span>Tình trạng giao dịch</Span></th>
        <th class="text-center"><Span>Trạng thái</Span></th>
        <th class="text-center"><Span>Phương thức thanh toán</Span></th>
        <th class="text-center"><Span>Yêu cầu </Span></th>
      </tr>
    </thead>
    <tbody>
      <?php
      $i = 0;
      $total = 0;
      foreach ($order_details as $key => $ord) {

        $i++;
      ?>

        <tr class="col 6" style="font-size: 15px;  font-family: 'Roboto';">
          <td class="text-center"><?php echo $i ?></td>
          <td class="text-center"><?php echo $ord['order_code'] ?></td>
          <td class="text-center"><?php echo $ord['order_date'] ?></td>
          <td class="text-center"><a href="<?php echo BASE_URL ?>/order/customer_orderdetails/<?php echo $ord['order_code'] ?>">Chi tiết</a></td>
          <td style="text-align: center;"><?php if ($ord['transaction'] == 1) {
                                            echo 'Đã thanh toán';
                                          } else {
                                            echo 'Chưa thanh toán';
                                          } ?></td>
          <td style="text-align: center;"><?php if ($ord['order_status'] == 0) {
                                            echo 'Đơn hàng mới';
                                          }
                                          if ($ord['order_status'] == 2) {
                                            echo 'Đã hủy';
                                          } else if ($ord['order_status'] == 4) {
                                            echo 'Đang vận chuyển';
                                          } ?></td>
          <td class="text-center"><?php  echo $ord['payment_methods'] ?></td>

          <td class="text-center">
            <?php if ($ord['cancel'] == 0) { ?>
              <form method="post" action="<?php echo BASE_URL ?>/order/canncel_order">
                <input type="hidden" name="code" value="<?php echo $ord['order_code'] ?>">
                <input type="hidden" name="customers_id" value="<?php echo $ord['customers_id'] ?>">
                <input type="hidden" value="1" name="cancel">
                <button type="submit" class="btn btn-danger btn-sm" style=" font-family: 'Roboto';">Hủy đơn hàng</button>
              </form>
            <?php } else if ($ord['order_status'] == 2 || $ord['order_status'] == 4) { ?>
              <p></p>
            <?php } else if ($ord['cancel'] == 1) { ?>

              <p>Đợi xác thực</p>
            <?php } ?>

          </td>
          <td>



          </td>
        </tr>
      <?php
      }
      ?>


      </form>

    </tbody>
  </table>
</div>