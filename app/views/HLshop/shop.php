<style>
  .font_fix {
    font-family: 'Roboto', sans-serif;
    font-size: 15px;
  }

  a {
    font-size: 15px !important;
    color: black;
    text-decoration: none;
  }

  h4 {
    font-family: 'Roboto', sans-serif;
    font-size: 18px;
  }
</style>

<div class="custom-border-bottom py-3">
  <div class="container">
    <div class="row" style="font-size: 30px; font-family: system-ui;">
      <div class="col-md-12 mb-0">
        <a href="<?php echo BASE_URL ?>/index/homepage">Trang chủ / </a>
        <strong class="text-black font_fix">Shop / Tất cả sản phẩm</strong>
      </div>
    </div>
  </div>
</div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">

<div class="container">
  <?php
  if (!empty($_GET['msg'])) {
    $msg = unserialize(urldecode($_GET['msg']));
    foreach ($msg as $key => $value) {
      echo '  <div class="container">
    <h3>Notification</h3>
    <div class="alert alert-success">
      <h4>' . $value . '</h4>
    </div> ';
    }
  }
      // Hàm so sánh giá của hai sản phẩm
    function comparePrices($a, $b)
    {
        return $a['product_price'] - $b['product_price'];
    }

    // Hàm so sánh tiêu đề theo bảng chữ cái tiếng Việt
    function compareTitles($a, $b)
    {
        return strcmp($a['product_title'], $b['product_title']);
    }

    // ...

    // Trước khi lặp, hãy kiểm tra xem người dùng có yêu cầu lọc sản phẩm không
    if (isset($_GET['sort'])) {
    $sort = $_GET['sort'];
    switch ($sort) {
        case 'price_desc':
            usort($list_product, 'comparePrices'); // Lọc theo giá từ cao đến thấp
            break;
        case 'price_asc':
            usort($list_product, function ($a, $b) {
                return comparePrices($b, $a); // Lọc theo giá từ thấp đến cao
            });
            break;
        case 'title_asc':
            usort($list_product, 'compareTitles'); // Lọc theo tiêu đề theo thứ tự bảng chữ cái tăng dần
            break;
        case 'title_desc':
            usort($list_product, function ($a, $b) {
                return compareTitles($b, $a); // Lọc theo tiêu đề theo thứ tự bảng chữ cái giảm dần
            });
            break;
        default:
            break;
    }
}
  ?>
  <!-- Hiển thị bộ lọc -->
<div class="container">
    <div class="row">
        <div class="col-md-12 mb-3">
            <strong class="text-black font_fix">Bộ lọc:</strong>
            <a href="?sort=price_desc" class="btn btn-primary">Giá giảm dần</a> |
            <a href="?sort=price_asc" class="btn btn-primary">Giá tăng dần</a> |
            <a href="?sort=" class="btn btn-primary">Bỏ lọc</a>
        </div>
    </div>
</div>

  <div class="row clearfix">
    <?php
    // Số sản phẩm hiển thị trên mỗi trang
    $productsPerPage = 12;
    // Tổng số sản phẩm
    $totalProducts = count($list_product);
    // Tính tổng số trang
    $totalPages = ceil($totalProducts / $productsPerPage);
    // Trang hiện tại (nếu không được xác định, mặc định là trang đầu tiên)
    $currentPage = isset($_GET['page']) ? $_GET['page'] : 1;
    // Đảm bảo trang hiện tại không vượt quá tổng số trang
    $currentPage = max(1, min($currentPage, $totalPages));
    // Vị trí bắt đầu của sản phẩm trong truy vấn
    $startIndex = ($currentPage - 1) * $productsPerPage;
    // Lấy danh sách sản phẩm dựa trên trang hiện tại
    $currentProducts = array_slice($list_product, $startIndex, $productsPerPage);

    foreach ($currentProducts as $key => $product) {
    ?>
      <div class="col-lg-3 col-md-4 col-sm-12">
        <input type="hidden" name="quanlity_product" value="1">
        <div class="card product_item">
          <div class="body">
            <div class="cp_img">
              <?php if ($product['status'] == 0) {
                echo '<span class="btn-info  btn-sm">Stopped Selling</span>';
              ?>
                <img class="mb-1 mx-3" width="180" height="200" src="<?php echo BASE_URL ?>/public/upload/product/<?php echo $product['product_image'] ?>" alt="Product" class="img-fluid">
              <?php
              } else { ?>
                <img class="mb-1 mx-3" width="180" height="200" src="<?php echo BASE_URL ?>/public/upload/product/<?php echo $product['product_image'] ?>" alt="Product" class="img-fluid">
                <div class="hover">
                  <a href="javascript:void(0);" class="btn-primary waves-effect">
                    <a class="btn-primary btn-sm waves-effect" href="<?php echo BASE_URL ?>/sanpham/chitietsanpham/<?php echo $product['product_id'] ?>"><span class="icon-shopping-cart"></span> Chi tiết</a>
                  </a>
                </div>
              <?php } ?>
            </div>
            <div class="product_details" style="font-family: system-ui;">
              <h4><?php echo $product['product_title'] ?></h4>
              <ul class="product_price list-unstyled">
                <li style="font-size: 19px; " class="new_price text-danger"><?php echo number_format($product['product_price'], 0, ',', '.') . ' VNĐ' ?></li>
              </ul>
              <div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php
    }
    ?>
  </div>

  <!-- Hiển thị phân trang -->
  <nav aria-label="Page navigation example">
    <ul class="pagination justify-content-center">
      <?php for ($i = 1; $i <= $totalPages; $i++) : ?>
        <li class="page-item <?php echo ($i == $currentPage) ? 'active' : ''; ?>"><a class="page-link" href="?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
      <?php endfor; ?>
    </ul>
  </nav>
</div>
