<head>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">
  <style>
  @import url('https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap');
  @import url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css');
  
  .title-section {
    margin-top: 50px;
    margin-bottom: 20px;
    background-color: #ccc;
    font-family: 'Roboto', sans-serif;
    color: #11006F;
  }

  a {
    font-size: 13px !important;
    color: #11006F;
    text-decoration: none;
    background-color: transparent;
  }
  
  .product_category, h2, h4, .new_price {
    font-family: 'Roboto', sans-serif;
  }
  
  .product_category {
    font-size: 24px;
  }
  
  h2 {
    font-size: 25px;
  }
  
  h4 {
    font-size: 15px;
  }
  
  .new_price {
    font-size: 14px;
  }
  .products{
    display: flex;
    justify-content: space-between;
    gap: 30px;
    flex-wrap: wrap;
  }

  .products div {
    flex: 0 0 calc(16.66% - 30px); /* Chiều rộng cụ thể của mỗi phần tử con */
    padding: 5px;
  }
  .bder {
    border-radius: 20px;
  }
  .img-product{
    margin-right: 30px;
  }
</style>

<div class="container">
  <div class="row">
    <div class=" hver title-section mb-5 col-12">
      <h2 class="text-uppercase">Bộ sưu tập</h2> 
    </div>
    <div class="row align-items-stretch">
      <div class="products">
    <?php
    foreach ($category as $key => $cate) {
    ?>
    <div class="col-lg-2 product">
      <a class="product_category" href="<?php echo BASE_URL ?>/sanpham/danhmuc/<?php echo $cate['category_product_id'] ?>"><?php echo $cate['category_product_title'] ?></a>
      <div class="">
        <img class="ml-2 img-product bder" width="100" height="100" style="margin-top: 5px; border-radi" src="<?php echo BASE_URL ?>/public/upload/category/<?php echo $cate['category_product_images'] ?>" alt="Product" class="img-fluid">
      </div>
    </div>
    <?php
    }
    ?>
      </div>
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="title-section mb-5 col-12">
      <h2 class="text-uppercase">Sản phẩm phổ biến</h2>
    </div>
  </div>
  <div class="row clearfix">
    <?php
    foreach ($list_product as $key => $product) {
    ?>
      <div class="col-lg-3 col-md-4 col-sm-12">
        <input type="hidden" name="quanlity_product" value="1">
        <div class="card product_item">
          <div class="body">
            <div class="cp_img">
              <?php if ($product['status'] == 0) {
                echo '<span class="btn-info btn-sm">Stopped Selling</span>';
              ?>
                <img class="mb-1 mx-3" width="180" height="200" src="<?php echo BASE_URL ?>/public/upload/product/<?php echo $product['product_image'] ?>" alt="Product" class="img-fluid">
              <?php
              } else { ?>
                <img class="mb-1 mx-3" width="180" height="200" src="<?php echo BASE_URL ?>/public/upload/product/<?php echo $product['product_image'] ?>" alt="Product" class="img-fluid">
                <div class="hover">
                  <a href="javascript:void(0);" class="btn-primary waves-effect">
                    <a class="btn-primary btn-sm waves-effect" href="<?php echo BASE_URL ?>/sanpham/chitietsanpham/<?php echo $product['product_id'] ?>"><span class="icon-shopping-cart"></span> Xem chi tiết</a>
                  </a>
                </div>
              <?php } ?>
            </div>
            <div class="product_details">
              <h4><?php echo $product['product_title'] ?></h4>
              <ul class="product_price list-unstyled">
                <li style="font-size: 19px;" class="new_price text-danger"><?php echo number_format($product['product_price'], 0, ',', '.') . ' VNĐ' ?></li>
              </ul>
              <div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php
    }
    ?>
  </div>
</div>
</head>