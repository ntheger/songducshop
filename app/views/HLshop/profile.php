<style>
    a {
    font-size: 18px !important;
    color: #11006F;
    text-decoration: none;
    background-color: transparent;
  }
  h5 {
    font-size: 18px;
  }
  h4 {
    font-size: 18px;
  }
</style>

<section style="background-color: #eee;">
  <div class="container py-5">
    <div class="row" style=" font-size: 18px; font-family: system-ui;">
      <div class="col">
        <nav aria-label="breadcrumb" class="bg-light rounded-3 p-3 mb-4">
          <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item"><a href="<?php echo BASE_URL ?>/index/homepage">Trang chủ</a></li>
            <li class="breadcrumb-item active" aria-current="page">Thông tin người dùng</li>
          </ol>
        </nav>
      </div>
    </div>
    <?php
    if (!empty($_GET['msg'])) {
      $msg = unserialize(urldecode($_GET['msg']));
      foreach ($msg as $key => $value) {
        echo '  <div class="container">
  <h3>Notification</h>
  <div class="alert alert-success">
    <h4>' . $value . '</h4> 
  </div> ';
      }
    }


    ?>
    <div class="row" style=" font-size: 15px; font-family: system-ui;">
      <div class="col-lg-4">
        <?php
        foreach ($customer as $key => $info) {
        ?>
          <div class="card mb-4">
            <div class="card-body text-center" style="font-family: system-ui;">
              <div class="profile-image">
                <img style="width: 150px; height: 150px;" src="<?php echo BASE_URL ?>/<?php echo $info['avatar']; ?>" alt="User Avatar">
              </div>
              <h5 class="my-3"><?php echo $info['customers_name']; ?></h5>
              <a href="<?php echo BASE_URL ?>/customer/edit_profile">
                <h5 class="my-3">Chỉnh sửa hồ sơ <span class="icon-edit"></span></h5>
              </a>
            </div>
          </div>
        <?php } ?>
      </div>
      <div class="col-lg-8">
        <div class="card mb-4">
          <div class="card-body" style=" font-family: system-ui;">
            <?php
            foreach ($customer as $key => $info) {
            ?>
              <div class="row">
                <div class="col-sm-4">
                  <p class="mb-0">Tên người dùng:</p>
                </div>
                <div class="col-sm-8">
                  <p class="text-muted mb-0"><?php echo $info['customers_name'] ?></p>
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-sm-4">
                  <p class="mb-0">Email:</p>
                </div>
                <div class="col-sm-8">
                  <p class="text-muted mb-0"><?php echo $info['email'] ?></p>
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-sm-4">
                  <p class="mb-0">Mật khẩu:</p>
                </div>
                <div class="col-sm-8">
                  <p class="text-muted mb-0">********</p>
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-sm-4">
                  <p class="mb-0">Số điện thoại:</p>
                </div>
                <div class="col-sm-8">
                  <p class="text-muted mb-0"><?php echo $info['phone'] ?></p>
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-sm-4">
                  <p class="mb-0">Địa chỉ:</p>
                </div>
                <div class="col-sm-8">
                  <p class="text-muted mb-0"><?php echo $info['address'] ?></p>
                </div>
              </div>
          </div>
        <?php } ?>
        </div>
      </div>
    </div>
  </div>
</section>