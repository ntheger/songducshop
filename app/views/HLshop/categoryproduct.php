<style>
      .font_fix {
      font-family: 'Roboto', sans-serif;
      font-size: 15px;
    }
    a {
      font-size: 15px !important;
      color: #11006F;
      text-decoration: none;
      background-color: transparent;
    }
    h4 {
      font-size: 15px;
    }
</style>
<?php
// Kiểm tra nếu biến $category_by_id tồn tại và không rỗng
if (isset($category_by_id) && !empty($category_by_id)) {
    // Lấy giá trị của biến $cate từ mảng đầu tiên trong $category_by_id
    $cate = reset($category_by_id);
}

// Lọc và sắp xếp sản phẩm nếu có yêu cầu
$filtered_products = $category_by_id;

if (isset($_GET['sort'])) {
    $sort_order = $_GET['sort'];
    if ($sort_order === 'low_to_high') {
        usort($filtered_products, function($a, $b) {
            return $a['product_price'] - $b['product_price'];
        });
    } elseif ($sort_order === 'high_to_low') {
        usort($filtered_products, function($a, $b) {
            return $b['product_price'] - $a['product_price'];
        });
    }
}

?>

<!-- Hiển thị tiêu đề trang dựa trên biến $cate -->
<div class="custom-border-bottom py-3">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-0"><a class="text-black" style="text-decoration: none;" href="<?php echo BASE_URL ?>/index/homepage">Trang chủ / </a>
                <?php if (isset($cate)) : ?>
                    <strong class="text-black font_fix">Shop / <?php echo $cate['category_product_title'] ?></strong>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">

<div class="container">
      <!-- Thêm các nút chức năng lọc và sắp xếp -->
  <div class="row mb-3">
    <div class="col-md-12">
      <div class="d-flex justify-content-between align-items-center">
        <div>
          <a href="<?php echo BASE_URL . $_SERVER['REQUEST_URI'] . '&sort=low_to_high'; ?>" class="btn btn-primary">Giá giảm dần</a>
          <a href="<?php echo BASE_URL . $_SERVER['REQUEST_URI'] . '&sort=high_to_low'; ?>" class="btn btn-primary">Giá tăng dần</a>
          <a href="?sort=" class="btn btn-primary">Bỏ lọc</a>
        </div>
      </div>
    </div>
  </div>
  <div class="row clearfix">
    <?php
    foreach ($filtered_products as $key => $product) {

    ?>
      <div class="col-lg-3 col-md-4 col-sm-12">

        <input type="hidden" name="quanlity_product" value="1">
        <div class="card product_item" style="
         font-family: system-ui;">

          <div class="body">
            <div class="cp_img">
              <?php if ($product['status'] == 0) {
                echo '<span class="btn-info  btn-sm">Stopped Selling</span>';
              ?>
                <img class="mb-1 mx-3" width="180" height="200" src="<?php echo BASE_URL ?>/public/upload/product/<?php echo $product['product_image'] ?>" alt="Product" class="img-fluid">
              <?php
              } else { ?>
                <img class="mb-1 mx-3" width="180" height="200" src="<?php echo BASE_URL ?>/public/upload/product/<?php echo $product['product_image'] ?>" alt="Product" class="img-fluid">
                <div class="hover">
                  <a href="javascript:void(0);" class="  btn-primary waves-effect ">
                    <a class=" btn-primary btn-sm waves-effect" href="<?php echo BASE_URL ?>/sanpham/chitietsanpham/<?php echo $product['product_id'] ?>"><span class="icon-shopping-cart"></span> Chi tiết</a>
                  </a>
                </div>

              <?php } ?>
            </div>
            <div class="product_details">
              <h4><?php echo $product['product_title'] ?></h4>
              <ul class="product_price list-unstyled">
                <li style="font-size: 19px; " class="new_price text-danger"><?php echo number_format($product['product_price'], 0, ',', '.') . ' VNĐ' ?></li>
              </ul>
              <div>
              </div>
            </div>

          </div>

        </div>
        <!-- </form> -->
      </div>
    <?php
    }
    ?>
  </div>
</div>