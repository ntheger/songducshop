<?php
// Include file Session.php
require_once 'system/lib/Session.php';

// Khởi tạo phiên sử dụng hàm init() trong class Session
Session::init();

// Kiểm tra xem người dùng đã đăng nhập chưa, nếu đã đăng nhập thì chuyển hướng đến trang chủ hoặc trang mong muốn.
if (!empty($_SESSION['is_logged_in']) && $_SESSION['is_logged_in'] === true) {
    header('Location: ' . BASE_URL . '/index/homepage');
    exit;
}

// Kiểm tra xem người dùng có gửi biểu mẫu đăng nhập hay không
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Đặt mã xác thực đăng nhập của bạn ở đây
    // Giả sử đăng nhập thành công, thiết lập biến phiên 'is_logged_in' thành true
    // Đăng nhập thành công, thiết lập biến phiên 'is_logged_in' thành true
    $_SESSION['is_logged_in'] = true;

    // Chuyển hướng đến trang mong muốn với thông báo thành công trong tham số truy vấn
    $success_message = "SD Shop xin chào! Chúc bạn mua hàng vui vẻ";
    header('Location: ' . BASE_URL . '/index/homepage?msg=' . urlencode(serialize([$success_message])));
    exit;
}
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">
    <style>
        .font-fix {
            font-family: Roboto;
            font-size: 15px;
        }
        a {
            font-size: 13px !important;
            color: #11006F;
            text-decoration: none;
            background-color: transparent;
        }
    </style>
</head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">

    <body>
    <?php
      // Kiểm tra xem có thông báo thành công từ tham số truy vấn không
      if (!empty($_GET['msg'])) {
          // Trích xuất thông báo từ tham số truy vấn và unserialize nó
          $msg = unserialize(urldecode($_GET['msg']));

          // Hiển thị thông báo
          foreach ($msg as $key => $value) {
              echo '<div class="container">
                      <h3>Notification</h3>
                      <div class="alert alert-success">
                          <h4>' . $value . '</h4> 
                      </div>
                  </div>';
          }
      }
      ?>
          <div class="custom-border-bottom py-3 font-fix">
   <div class="container">
     <div class="row font-fix">
       <div class="col-md-12 mb-0"><a href="<?php echo BASE_URL ?> /index/homepage">Trang chủ</a> <span class="mx-2 mb-0">/</span> <strong class="text-black">Đăng nhập</strong></div>
     </div>
   </div>
 </div>
 <div class="container">
   <div class="row">
     <div class="col-md-8 font-fix">

      <form autocomplete="off" action="<?php echo BASE_URL ?>/customer/customer_login" method="post">
         <div class="p-3 p-lg-5 border">

           <div class="form-group row">
             <div class="col-md-12">
               <label for="c_email" class="text-black font-fix">Tên đăng nhập <span class="text-danger">*</span></label>
               <input type="email" class="form-control font-fix" id="c_email" name="username" placeholder="">
             </div>
           </div>
           <div class="form-group row">
             <div class="col-md-12">
               <label for="c_subject" class="text-black font-fix">Mật khẩu <span class="text-danger">*</span> </label>
               <input type="password" class="form-control font-fix" id="c_subject" name="password">
             </div>
           </div>
        
           <a class="font-fix" href="<?php echo BASE_URL ?>/customer/forgotpassword">Quên mật khẩu</a>
           <div class="form-group row">

             <div class="col-lg-3">
               <input type="submit" class="btn btn-info btn-lg btn-block font-fix" value="Đăng nhập">
             </div>
             <div class="col-lg-3">


               <a href="<?php echo BASE_URL ?>/customer/customer_signup" class="btn btn-info btn-lg btn-block font-fix" value="Đăng nhập">Đăng ký</a>
             </div>
           </div>
         </div>


       </form>
     </div>
     <div class="col-md-4 ml-auto" style="  font-family: 'Roboto';">
       <div class="p-4 border mb-3">
         <span class="d-block text-info h6 text-uppercase font-fix">Tỉnh Bến Tre</span>
         <h5 class="mb-0 font-fix">Đồng Khởi, Bến Tre</h5>
       </div>
       <div class="p-4 border mb-3">
         <span class="d-block text-info h6 text-uppercase font-fix">Thành phố Hà Nội</span>
         <h5 class="mb-0 font-fix">Hà Đông - Hà Nội</h5>
       </div>
       <div class="p-4 border mb-3">
         <span class="d-block text-info h6 text-uppercase font-fix">Thành phố Hồ Chí Minh</span>
         <h5 class="mb-0 font-fix">Tôn Thất Thuyết, Phường 3 Quận 4 , Hồ Chí Minh</h5>
       </div>

     </div>
   </div>
 </div>
    </body>
    </html>