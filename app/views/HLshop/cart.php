<style>
      .font_fix {
      font-family: 'Roboto', sans-serif;
      font-size: 15px  !important;
    }
    a {
      font-size: 15px !important;
      color: #11006F;
      text-decoration: none;
      background-color: transparent;
    }
    .fixh4 {
      font-size: 18px;
    }
  </style>
<div class="bg-light py-3">
  <div class="container">
    <div class="row" style=" font-size: 15px;
         font-family: 'Roboto'">
      <div class="col-md-12 mb-0 font_fix"><a href="/">Trang chủ</a> <span class="mx-2 mb-0">/</span> <strong class="text-black">Giỏ hàng</strong></div>
    </div>
  </div>
</div>
<div>
  <?php
  if (!empty($_GET['msg'])) {
    $msg = unserialize(urldecode($_GET['msg']));
    foreach ($msg as $key => $value) {
      echo '  <div class="container">
      <h3>Notification</h>
      <div class="alert alert-success">
        <h4>' . $value . '</h4> 
      </div> ';
    }
  }

  ?>
</div>

<div class="container" style=" font-family: system-ui;">
  <div class="row mb-5">
    <?php
    if (isset($_SESSION['addtocart'])) {

    ?>
      <form class="col-md-12" action="<?php echo BASE_URL ?>/cart/updatecart" method="post">

        <div class="site-blocks-table">

          <table class="table table-bordered">
            <thead>
              <tr class="">
                <th class="product-thumbnail font_fix">Ảnh</th>
                <th class="product-name font_fix">Sản Phẩm</th>
                <th class="product-thumbnail font_fix">Size</th>

                <th class="product-price font_fix">Giá tiền</th>
                <th class="product-quantity font_fix">Số Lượng</th>
                <th class="product-total font_fix">Tổng</th>
                <th class="product-remove font_fix">Cập nhật</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $total = 0;
              foreach ($_SESSION['addtocart'] as $key => $value) {
                $subtotal = $value['quanlity_product'] * $value['price_product'];
                //sau moi lan lap cong moi thu thanh tien lai
                $total += $subtotal;
              ?>
                <tr style="font-size: 15px;">

                  <td class="product-thumbnail">
                    <img style="width: 100px; height: 100px" src="<?php echo BASE_URL ?>/public/upload/product/<?php echo $value['image_product'] ?>" alt="Image" class="img-fluid">
                  </td>
                  <td class="product-name">
                    <h2 class="text-black fixh4"><?php echo $value['title_product'] ?></h2>
                  </td>
                  <td class="product-name">

                    <select value="" class="form-select" name="size_product[<?php echo $value['size_product'] ?>]" aria-label="Default select example">
                      <option selected><?php echo $value['size_product'] ?></option>
                      <?php if ($value['size_product']=="M" && $value['quanlity_product']>=1){
                          echo '<option value="M">M</option>';
                      } ?>
                      <?php if ($value['size_product']=="S" && $value['quanlity_product']>=1){
                          echo '<option value="S">S</option>';
                      } ?>
                      <?php if ($value['size_product']=="L" && $value['quanlity_product']>=1){
                          echo '<option value="L">L</option>';
                      } ?>
                      
                      
                    </select>


                  </td>
                  <td><?php echo number_format($value['price_product'], 0, ',', '.') . 'VND' ?></td>
                  <td>
                    <div class="input-group mb-3" style="max-width: 120px;">
                      <div class="input-group-prepend">
                        <button class="btn btn-outline-info js-btn-minus btn-sm" type="button">&minus;</button>
                      </div>
                      <?php foreach ($productbyid as $key => $product) {

                      ?>
                        
                        <input type="number" value="<?php echo $value['quanlity_product'] ?>" name="qty[<?php echo $value['size_product'] ?>] " placeholder="" min="0" max="<?php echo $product['qty'] ?>" aria-label="Example text with button addon" aria-describedby="button-addon1" class="form-control btn-sm">
                        <div class="input-group-append">
                          <button class="btn btn-outline-info js-btn-plus btn-sm " type="button">&plus;</button>
                        </div>
                      <?php } ?>

                    </div>

                  </td>
                  <td><?php echo number_format($subtotal, 0, ',', '.') . 'VND' ?></td>
                  <td>


                    <button type="submit" name="delete_cart[<?php echo $value['size_product'] ?>]" value=" <?php echo $value['id_product'] ?>" class="btn btn-sm btn-danger mb-1 ">Xóa</button>
                    <button type="submit" name="update_cart[<?php echo $value['size_product'] ?>]" value="<?php echo $value['id_product'] ?>" class="btn btn-sm btn-info">Cập nhật</button>


                  </td>

                </tr>
              <?php } ?>
            </tbody>
          </table>

        </div>


      </form>
  </div>

  <div class="row" style=" font-family: system-ui;">
    <div class="col-md-6">
      <div class="row mb-5">
        <div class="col-md-6 mb-3 mb-md-0">
          <a href="<?php echo BASE_URL ?>/sanpham/tatca" class="btn btn-info btn-sm btn-block">Tiếp tục mua sắm</a>
        </div>

      </div>
    </div>
    <div class="col-md-6 pl-5">
      <div class="row justify-content-end">
        <div class="col-md-7">
          <div class="row">
            <div class="col-md-12 text-right border-bottom mb-5">
              <h3 class="text-black h4 text-uppercase" style=" font-family: system-ui;">Tổng giỏ hàng</h3>
            </div>
          </div>
          <div class="row mb-5">
            <div class="col-md-6">
              <span class="text-black" style=" font-size: 25px; font-family: system-ui;">Tổng tiền:</span>
            </div>
            <div class="col-md-6 text-right" style=" font-size: 25px; font-family: system-ui;">
              <strong class="text-danger"><?php echo number_format($total, 0, ',', '.') . 'VND' ?></strong>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <a class="btn btn-info btn-lg btn-block" href='<?php echo BASE_URL ?>/cart/dathang'>Tiếp tục thanh toán</a>

            </div>
          </div>
        <?php } ?>
        </div>

      </div>
    </div>
  </div>
</div>