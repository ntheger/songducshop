<style>
  .font_fix {
    font-size: 15px;
    font-family: 'Roboto', sans-serif;
  }
  h4 {
    font-size: 16px;
  }
  h2 {
    font-size: 18px;
  }
  a {
      font-size: 15px !important;
      color: #11006F;
      text-decoration: none;
      background-color: transparent;
    }
</style>


<div class="custom-border-bottom py-3">
  <div class="container" style=" font-size: 18px; font-family: 'Roboto';">
    <div class="row">
      <div class="col-md-12 mb-0"><a href="/">Trang chủ</a> <span class="mx-2 mb-0">/</span> <strong class="text-black font_fix">Liên hệ</strong></div>
    </div>
  </div>
</div>

<?php
if (!empty($_GET['msg'])) {
  $msg = unserialize(urldecode($_GET['msg']));
  foreach ($msg as $key => $value) {
    echo '  <div class="container">
      <h3>Notification</h>
      <div class="alert alert-success">
        <h4>' . $value . '</h4> 
      </div> ';
  }
}

?>

<div class="container" style=" font-family: system-ui;">
  <div class="row">
    <div class="col-md-12">
      <h2 class="h3 mb-3 text-black" style="font-family: 'Roboto', sans-serif;">Thông tin liên lạc</h2>
    </div>
    <div class="col-md-8">

      <form action="<?php echo BASE_URL ?>/contact/insert_contact" method="post">
        <?php
        foreach ($customer as $key => $info) {
        ?>
          <div class="p-3 p-lg-5 border">
            <div class="form-group row">
              <div class="col-md-12">
                <label for="c_fname" class="text-black font_fix">Tên <span class="text-danger">*</span></label>
                <input type="text" class="form-control font_fix" id="c_fname" name="name" value="<?php echo $info['customers_name'] ?> ">
              </div>
            </div>
            <input type="hidden" name="customers_id" class="form-control font_fix" id="c_fname" name="name" value="<?php echo $info['customers_id'] ?>">
            <div class="form-group row">
              <div class="col-md-12">
                <label for="c_email" class="text-black font_fix">Email <span class="text-danger">*</span></label>
                <input type="email" class="form-control font_fix" id="c_email" name="email" value="<?php echo $info['email'] ?>" placeholder="">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <label for="c_email" class="text-black font_fix">Số điện thoại <span class="text-danger">*</span></label>
                <input type="phone" class="form-control font_fix" id="c_email" name="phone" value="<?php echo $info['phone'] ?>" placeholder="">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-12">
                <label for="c_subject" class="text-black font_fix">Tiêu đề </label>
                <input type="text" class="form-control font_fix" id="c_subject" name="subject" required>
              </div>
            </div>

            <div class="form-group row">
              <div class="col-md-12">
                <label for="c_message" class="text-black font_fix">Tin nhắn </label>
                <textarea require name="message" id="c_message" cols="30" rows="7" class="form-control font_fix" required></textarea>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-lg-3">
                <input type="submit" name="contact" class="btn btn-info btn-lg btn-block font_fix" value="Submit">
              </div>
            </div>
          </div>
        <?php } ?>
      </form>
    </div>
    <div class="col-md-4 ml-auto" style="font-size: 13px; font-family: 'Roboto';">
      <div class="p-4 border mb-3">
        <span class="d-block text-info h6 text-uppercase font_fix">Tỉnh Bến Tre</span>
        <h5 class="mb-0 font_fix">Đồng Khởi, Bến Tre</h5>
      </div>
      <div class="p-4 border mb-3">
        <span class="d-block text-info h6 text-uppercase font_fix">Thành phố Hà Nội</span>
        <h5 class="mb-0 font_fix">Hà Đông - Hà Nội</h5>
      </div>
      <div class="p-4 border mb-3">
        <span class="d-block text-info h6 text-uppercase font_fix">Thành phố Hồ Chí Minh</span>
        <h5 class="mb-0 font_fix">Tôn Thất Thuyết, Phường 3 Quận 4 , Hồ Chí Minh</h5>
      </div>

    </div>
  </div>
</div>