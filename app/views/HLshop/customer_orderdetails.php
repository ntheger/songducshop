<head>
  <style>
      .font_fix {
      font-family: 'Roboto', sans-serif;
      font-size: 15px;
    }
    a {
      font-size: 13px !important;
      color: #11006F;
      text-decoration: none;
      background-color: transparent;
    }
    h1 {
      font-size: 25px;
    }
  </style>

<div class="custom-border-bottom py-3">
  <div class="container">
    <div class="row">
      <?php

      foreach ($order_details as $key => $ord) {
        $id_customer = $ord['customers_id'];
      }
      ?>
      <div class="col-md-12 mb-0"><a href="<?php echo BASE_URL ?>/order/customer_order/<?php echo  $id_customer ?>">Đặt hàng</a> <strong class="text-black font_fix">/ Chi tiết đặt hàng</strong></div>

    </div>

  </div>
</div>
<div class="container responsive " style="  font-family: system-ui;">
  <h1 class="m-2" style="text-align: center;"> Đơn đặt hàng của bạn</h1>

  <table class="table table-striped font_fix">
    <thead>
      <tr class="font_fix">
        <th class="text-center"><Span>ID</Span></th>
        <th class="text-center"><Span>Tên sản phẩm</Span></th>
        <th class="text-center"><Span>Hình ảnh</Span></th>

        <th class="text-center"><Span>Số lượng</Span></th>
        <th class="text-center"><Span>Size</Span></th>
        <th class="text-center"><Span>Giá tiền</Span></th>
        <th class="text-center"><Span>Tổng tiền sản phẩm </Span></th>
      </tr>
    </thead>
    <tbody>
      <?php
      $i = 0;
      $total = 0;
      foreach ($order_details as $key => $ord) {
        $total += $ord['order_product_quanlity'] * $ord['product_price'];
        $i++;
      ?>

        <tr class="col 6 font_fix">
          <td class="text-center"><?php echo $i ?></td>
          <td class="text-center"><?php echo $ord['product_title'] ?></td>
          <td class="text-center"><img width='100px' height='100px' src="<?php echo BASE_URL ?>/public/upload/product/<?php echo $ord['product_image'] ?>"></td>
          <td class="text-center"><?php echo $ord['order_product_quanlity'] ?></td>
          <td class="text-center"><?php echo $ord['size_product'] ?></td>
          <td class="text-center"><?php echo number_format($ord['product_price'], 0, ',', '.') . 'VND' ?></td>
          <td class="text-center"><?php echo number_format(($ord['order_product_quanlity'] * $ord['product_price']), 0, ',', '.') . 'VND' ?></td>


        </tr>
      <?php
      }
      ?>

      <tr>
        <td>
          <form action="<?php echo BASE_URL ?>/index/thanhtoanvnpay" method="post">
            <input type="hidden" name="code" value="<?php echo $ord['order_code'] ?>">
            <input type="hidden" name="custommer_id" value="<?php echo $ord['customers_id'] ?>">
            <input type="hidden" name="total_order" value="<?php echo $total * 100 ?>">
            <button type="submit" name="redirect" class="btn btn-warning btn-sm btn-block text-dark " style=" font-family: system-ui;"> Thanh toán bằng vnpay</button>

          </form>
        </td>
        <td colspan="10" align="right"><span class="text-danger" style="  font-family: system-ui;"> Tổng tiền đặt hàng: </span> <?php echo number_format($total, 0, ',', '.') . 'VND' ?></td>
      </tr>
      </form>

    </tbody>
  </table>
</div>
</head>
