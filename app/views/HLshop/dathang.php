<style>
   * {
     font-family: 'Roboto', sans-serif;
   }
   .font_fix {
      font-family: 'Roboto', sans-serif;
      font-size: 15px;
    }
    a {
      font-size: 15px !important;
      color: black;
      text-decoration: none;
    }
    h4 {
      font-family: 'Roboto', sans-serif;
      font-size: 18px;
    }
</style>
<div class="site-wrap">
  <div class="bg-light py-3">
    <div class="container">
      <div class="row"style="font-size: 30px;font-family: system-ui;">
        <div class="col-md-12 mb-0"><a href="<?php echo BASE_URL ?>/index/homepage">Trang chủ</a><span class="mx-2 mb-0 font_fix">/ Thủ tục thanh toán</span>
          <span class="mx-2 mb-0"></span>
          <div>
            <?php
            if (!empty($_GET['msg'])) {
              $msg = unserialize(urldecode($_GET['msg']));
              foreach ($msg as $key => $value) {
                echo '  <div class="container">
      <h3>Notification</h>
      <div class="alert alert-success">
        <h4>' . $value . '</h4> 
      </div> ';
              }
            }

            ?>
          </div>

        </div>
      </div>
    </div>
  </div>
  <?php

  if (isset($_SESSION['addtocart'])) {

  ?>

    <div class="container font_fix">
      <div class="row">
        <div class="col-md-6 mb-4 mb-md-0" style=" font-family: system-ui;">
          <h2 class="h3 mb-3 text-info">Chi tiết thanh toán</h2>

          <form name="formoder" action="<?php echo BASE_URL ?>/checkout/checkout" method="POST" autocomplete="off">
            <?php
            foreach ($customer as $key => $info) {
            ?>
              <div class="p-3 p-lg-5 border">



                <div class="form-group row" style=" font-family: 'Roboto';">
                  <div class="col-md-12">
                    <label for="c_companyname" class="text-black">Tên người dùng </label>
                    <input type="text" class="form-control font_fix" id="c_companyname" name="name" value="<?php echo $info['customers_name'] ?>">
                  </div>
                </div>
                <input type="hidden" name="customers_id" class="form-control font_fix" id="c_fname" name="name" value="<?php echo $info['customers_id'] ?>">
                <div class="form-group row">
                  <div class="col-md-12">
                    <label for="c_address" class="text-black">Địa chỉ <span class="text-danger">*</span></label>
                    <input type="text" class="form-control font_fix" id="c_address" name="address" value="<?php echo $info['address'] ?>">
                  </div>
                </div>




                <div class="form-group row mb-5" style="font-family: 'Roboto';">
                  <div class="col-md-12">
                    <label for="c_email_address" class="text-black">Địa chỉ Email <span class="text-danger">*</span></label>
                    <input type="text" class="form-control text-dark" id="c_email_address" name="email" value="<?php echo $info['email'] ?>">
                  </div>
                  <div class="col-md-12">
                    <label for="c_phone" class="text-black">Phone <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="c_phone" name="phone" value="<?php echo $info['phone'] ?>">
                  </div>
                  <div class="col-md-12">
                    <label for="c_phone" class="text-black">Phương thức thanh toán<span class="text-danger">*</span></label>
                    <select class="form-control" Name="method_payment" id="exampleFormControlSelect1">
                      <option value="Online">Thanh toán Online</option>
                      <option value="Tiền mặt">Thanh toán bằng tiền mặt</option>

                    </select>
                  </div>
                </div>

                <div class="form-group" style=" font-family: 'Roboto';">
                  <label for="c_order_notes" class="text-black">Ghi chú đặt hàng</label>
                  <textarea name="note" id="c_order_notes" cols="30" rows="5" class="form-control" placeholder="Hãy điền ghi chú của bạn"></textarea>
                </div>
              </div>
              <input type="submit" class=" btn-primary text-center m-2 b-1" class="bg-danger" name="formoder" value="Thanh toán">
            <?php } ?>
          </form>

        </div>
        <div class="col-md-6">
          <div class="row mb-5">
            <div class="col-md-12">
              <h3 class=" mb-3 text-info" style=" font-family: 'Roboto';">Đơn đặt hàng của bạn</h3>
              <div class=" p-lg-5 border">
                <table class="table site-block-order-table mb-5"style="font-size: 15px; font-family: 'Roboto';">
                  <thead>
                    <th style="font-size: 20px; font-family: 'Roboto';">Sản phẩm</th>
                    <th style="font-size: 20px; font-family: 'Roboto';">Tổng cộng</th>
                  </thead>

                  <tbody>
                    <?php

                    $total = 0;
                    foreach ($_SESSION['addtocart'] as $key => $value) {
                      $subtotal = $value['quanlity_product'] * $value['price_product'];
                      //sau moi lan lap cong moi thu thanh tien lai
                      $total += $subtotal;
                    ?>
                      <tr>
                        <td style="font-size: 20px;  font-family: 'Roboto';"><?php echo $value['title_product'] ?><strong class="mx-2"></strong>X <?php echo $value['quanlity_product'] ?> <?php echo $value['size_product'] ?></td>
                        <td style="font-size: 20px;  font-family: 'Roboto';"><?php echo number_format($value['price_product'], 0, ',', '.') . 'VNĐ' ?></td>
                      </tr>

                      <tr>
                        <td class="text-dark font-weight-bold" style="font-size: 20px; font-family: 'Roboto';"><strong>Tổng sản phẩm trong giỏ hàng</strong></td>
                        <td style="font-size: 20px;"> <?php echo $value['quanlity_product'] ?> <span> Sản phẩm</span>

                      </tr>
                    <?php } ?>
                    <tr>
                      <td class="text-info font-weight-bold" style="font-size: 20px;  font-family: 'Roboto';"><strong>Tổng tiền phải thanh toán:</strong></td>
                      <td class="text-info font-weight-bold" style="font-size: 20px;  font-family: 'Roboto';"><strong><?php echo number_format($total, 0, ',', '.') . 'VNĐ' ?></strong></td>
                    </tr>

                  </tbody>

                </table>
                <div class="form-group">

                </div>

              </div>
            </div>
          </div>

        </div>

      </div>

    </div>

  <?php } ?>
</div>