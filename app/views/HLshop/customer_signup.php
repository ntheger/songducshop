<?php
if (!empty($_GET['msg'])) {
  $msg = unserialize(urldecode($_GET['msg']));
  foreach ($msg as $key => $value) {
    echo '  <div class="container">
    <h3>Notification</h>
    <div class="alert alert-success">
      <h4>' . $value . '</h4> 
    </div> ';
  }
}

foreach ($customer as $key => $value) {
  $email = $value['email'];
}
?>
<style>
  .font_fix {
    font-family: 'Roboto', sans-serif;
    font-size: 15px;
  }
  a {
    font-size: 13px !important;
    color: #11006F;
    text-decoration: none;
    background-color: transparent;
  }
</style>

<div class="custom-border-bottom py-3">
  <div class="container">
    <div class="row font_fix">
      <div class="col-md-12 mb-0"><a href="<?php echo BASE_URL ?> /index/homepage">Trang chủ</a> <span class="mx-2 mb-0">/</span> <strong class="text-black">Đăng ký</strong></div>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h2 class="h3 mb-3 text-black font_fix">Nhập đầy đủ thông tin bên dưới</h2>
    </div>
    <div class="col-md-8">

      <form action="<?php echo BASE_URL ?>/customer/insert_signup" method="post">

        <div class="p-3 p-lg-5 border font_fix">
          <div class="form-group row">
            <div class="col-md-12 ">
              <label for="c_fname" class="text-black">Tên đăng ký<span class="text-danger">*</span></label>
              <input type="text" class="form-control" id="c_fname" name="name">
            </div>
          </div>
          <div class="form-group row font_fix">
            <div class="col-md-12">

              <label for="c_email" class="text-black">Email login <span class="text-danger">*</span></label>

              <input type="email" class="form-control" id="c_email" name="email" placeholder="">


            </div>
          </div>
          <div class="form-group row font_fix">
            <div class="col-md-12">
              <label for="c_subject" class="text-black ">Mật khẩu <span class="text-danger">*</span> </label>
              <input type="password" class="form-control" id="c_subject" name="password">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-12">
              <label for="c_subject" class="text-black ">Phone </label>
              <input type="text" class="form-control " id="c_subject" name="phone">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-12">
              <label for="c_subject" class="text-black">Địa chỉ </label>
              <input type="text" class="form-control" id="c_subject" name="address">
            </div>
          </div>




          <div class="form-group row font_fix">
            <div class="col-lg-3">
              <input type="submit" name="signup" class="btn btn-info btn-lg btn-block font_fix" value="Đăng ký">
            </div>
          </div>
        </div>
      </form>
    </div>
    <div class="col-md-4 ml-auto" style="  font-family: 'Roboto';">
       <div class="p-4 border mb-3">
         <span class="d-block text-info h6 text-uppercase font_fix">Tỉnh Bến Tre</span>
         <h5 class="mb-0 font_fix">Đồng Khởi, Bến Tre</h5>
       </div>
       <div class="p-4 border mb-3">
         <span class="d-block text-info h6 text-uppercase font_fix">Thành phố Hà Nội</span>
         <h5 class="mb-0 font_fix">Hà Đông - Hà Nội</h5>
       </div>
       <div class="p-4 border mb-3">
         <span class="d-block text-info h6 text-uppercase font_fix">Thành phố Hồ Chí Minh</span>
         <h5 class="mb-0 font_fix">Tôn Thất Thuyết, Phường 3 Quận 4 , Hồ Chí Minh</h5>
       </div>

     </div>
  </div>
</div>