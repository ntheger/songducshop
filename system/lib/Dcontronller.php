<?php
//hien thi view va goi model tra lai toan bo du lieu cua view
  class Dcontroller {
    protected $load;

    public function __construct() {
        $this->load = new Load();
    }

    public function loadModel($model) {
        $modelPath = 'models/' . $model . '_model.php';

        if (file_exists($modelPath)) {
            require_once($modelPath);
            $modelClass = ucfirst($model) . '_model';
            return new $modelClass();
        } else {
            die("Model not found: $model");
        }
    }

    public function loadView($viewName, $data = array()) {
        extract($data);
        require_once('views/' . $viewName . '.php');
    }
  }
?>