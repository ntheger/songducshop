<?php
class Session {
    public static function init() {
        // Kiểm tra trạng thái phiên, nếu chưa tồn tại thì mới bắt đầu phiên
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
    }

    public static function set($key, $val) {
        self::init();
        $_SESSION[$key] = $val;
    }

    public static function get($key) {
        self::init();
        if (isset($_SESSION[$key])) {
            return $_SESSION[$key];
        } else {
            return false;
        }
    }

    public static function chesksession_customer() {
        self::init();
        if (self::get('admin') == false && self::get('host') == false) {
            header("Location:" . BASE_URL . "/index/homepage");
            exit;
        } else if (self::get('admin') == false && self::get('user') == false) {
            header("Location:" . BASE_URL . "/index/homepage");
            exit;
        }
    }

    public static function chesksession() {
        self::init();
        if (self::get('user') == false) {
            self::destroy();
            header("Location:" . BASE_URL . "/customer/customer_signup");
            exit;
        }
    }

    public static function destroy() {
        self::init();
        session_destroy();
    }

    public static function unset($key) {
        self::init();
        unset($_SESSION[$key]);
    }
}
?>